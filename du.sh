#! /bin/bash

get_sizes='$0 !~ /^".*/ && length($4) {
    if (!($1 in used)) {
	print($4);
	used[$1] = 1
     }
} '

print_size () {
  children_sizes=$(ls -ARQgoi $1 | awk "$get_sizes")
  my_size=$(ls -AQdgoi $1 | awk "$get_sizes")

  for child_size in $children_sizes; do
    my_size=$(( $my_size + $child_size ));
  done

  printf "%-10s %s %s\n" $my_size $1

}


for f in $(find . -type d  -mindepth 0 2>/dev/null ); do
    print_size $f
done
